# Orden de la ejecución de los playbooks

ansible-playbook -i hosts users.yml  
ansible-playbook -i hosts install-k8s.yml  
ansible-playbook -i hosts master.yml  
ansible-playbook -i hosts join-workers.yml  

## Nota importante sobre la instalación del dashboard

Para poder acceder al dashboard de Kubernetes, al crear la cuenta de servicio, a partir de la 1.24, ya no se crea el token automaticamente, en la siguiente URL se explica cómo solucionarlo:
<https://itnext.io/big-change-in-k8s-1-24-about-serviceaccounts-and-their-secrets-4b909a4af4e0>

Esto ya está añadido en el YAML: yaml-kubernetes/kubernetes-dashboard-service-np.yml

## Instalación Helm

Helm realmente es una aplicación para ejecutar desde nuestro equipo/servidor. Para instalarlo sólo hay que hacer lo siguiente:

1. Nos descargamos la versión que queramos [aquí](https://github.com/helm/helm/releases)
2. Desempaquetamos el fichero `tar -zxvf helm-*-linux-amd64.tar.gz`
3. Movemos el binario a la carpeta de ejecutables `mv linux-amd64/helm /usr/local/bin/helm`

## Instalación Istio

Este componente se puede instalar de igual manera que Helm, en la siguiente [URL](https://istio.io/latest/docs/setup/getting-started/) viene explicado cómo instalarlo. La instalación tardará más o menos según la velocidad de tu internet. Hay varios perfiles para instalarlo, ya según las necesidades, utilizaremos una u otra, para el laboratorio que me he montado lo he hecho con `demo`.

Para el tema del fichero de `yaml-kubernetes/istio-services.yaml`, esto sólo crea los NodePort para cada componente, en teoría Istio los crea automaticamente según he visto en tutoriales, pero en mi caso no lo ha creado, quizás por la poca capacidad del cluster. Simplemente habría que desplegar cada uno de los componentes.
